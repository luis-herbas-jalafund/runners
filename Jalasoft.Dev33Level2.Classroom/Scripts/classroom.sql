SET IMPLICIT_TRANSACTIONS OFF
GO

USE master
GO

DROP DATABASE ClassDb
GO

CREATE DATABASE ClassDb
GO

USE ClassDb
GO

CREATE TABLE Jalasoft_Group(
	Id INT IDENTITY (1,1) PRIMARY KEY,
	Name VARCHAR(50),
	Description VARCHAR(100),
	MaxUsersQuantity SMALLINT
);
GO

CREATE TABLE Jalasoft_User (
	Id INT IDENTITY (1,1) PRIMARY KEY,
	FirstName VARCHAR(25),
	LastName VARCHAR(25),
	Gender INT,
	DateBorn DATETIME,
	Email VARCHAR(50),
	GroupId INT,
	CONSTRAINT FK_Group FOREIGN KEY (GroupId)
	REFERENCES Jalasoft_Group(Id)
);
GO

INSERT INTO Jalasoft_Group VALUES('DEV33 Level1', 'Training in Commercial Software Development1', 10);
INSERT INTO Jalasoft_Group VALUES('DEV33 Level2', 'Training in Commercial Software Development2', 5);
INSERT INTO Jalasoft_Group VALUES('DEV33 Level3', 'Training in Commercial Software Development3', 4);
GO

SELECT * FROM Jalasoft_Group;
GO