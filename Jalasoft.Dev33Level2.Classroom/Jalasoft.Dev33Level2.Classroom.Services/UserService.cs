﻿using FluentValidation.Results;
using Jalasoft.Dev33Level2.Classroom.Models;
using Jalasoft.Dev33Level2.Classroom.Models.Validators;
using Jalasoft.Dev33Level2.Classroom.Repositories;
using Jalasoft.Dev33Level2.Classroom.Repositories.Interfaces;
using Jalasoft.Dev33Level2.Classroom.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jalasoft.Dev33Level2.Classroom.Services
{
    public class UserService : IService<User>
    {
        IRepository<User> repository;
        UserValidator validator;
        public UserService(IRepository<User> repository)
        {
            this.repository = repository;
            validator = new UserValidator();
        }
        public User Create(User elem)
        {
            ValidationResult result = validator.Validate(elem);
            if (result.IsValid)
            {
                return repository.Create(elem);
            }
            string errorMessage = string.Empty;
            result.Errors.ForEach(e => errorMessage+= $"\n {e}");
            throw new Exception(errorMessage);
        }

        public bool Delete(int id)
        {
            var check = repository.GetById(id);
            if (check == null)
            {
                throw new Exception("User does not exist.");
            }
            return repository.Delete(id);
        }

        public List<User> GetAll()
        {
            return repository.GetAll();
        }

        public User GetById(int id)
        {
            var res = repository.GetById(id);
            if (res == null)
            {
                throw new Exception("User not Found");
            }
            return res;
        }

        public User Update(User elem)
        {
            var check = repository.GetById(elem.Id);
            
            if (check == null)
            {
                throw new Exception("User does not exist.");
            }
            ValidationResult result = validator.Validate(elem);
            if (result.IsValid)
            {
                return repository.Update(elem);
            }
            string errorMessage = string.Empty;
            result.Errors.ForEach(e => errorMessage += $"\n {e}");
            throw new Exception(errorMessage);

        }
    }
}
