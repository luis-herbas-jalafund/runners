﻿using FluentValidation.Results;
using Jalasoft.Dev33Level2.Classroom.Models;
using Jalasoft.Dev33Level2.Classroom.Models.Validators;
using Jalasoft.Dev33Level2.Classroom.Repositories;
using Jalasoft.Dev33Level2.Classroom.Repositories.Interfaces;
using Jalasoft.Dev33Level2.Classroom.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace Jalasoft.Dev33Level2.Classroom.Services
{

    
    public class GroupService : IService<Group>
    {
        IRepository<Group> repository;
        GroupValidator validator;
        public GroupService( IRepository<Group> repository)
        {
            this.repository = repository;
            validator = new GroupValidator();
        }

        public List<Group> GetAll()
        {
            return repository.GetAll();
        }

        public Group GetById(int id)
        {
            var group = repository.GetById(id);
            if (group == null)
            {
                throw new Exception("Group not Found");
            }
            return group;
        }

        public Group Create(Group group)
        {
            ValidationResult result = validator.Validate(group);
            if (result.IsValid)
            {
                return repository.Create(group);
            }
            throw new Exception("You must specify the description and name");
        }

        public Group Update(Group group)
        {
            var check = repository.GetById(group.Id);
            if (check == null)
            {
                throw new Exception("Group does not exist.");
            }
            ValidationResult result = validator.Validate(group);
            if (result.IsValid)
            {
                return repository.Update(group);
            }
            throw new Exception("You must specify the description and name");

        }

        public bool Delete(int id)
        {
            var check = repository.GetById(id);
            if (check == null)
            {
                throw new Exception("Group does not exist.");
            }
            return repository.Delete(id);
        }

    }
}
