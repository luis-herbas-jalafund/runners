﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jalasoft.Dev33Level2.Classroom.Services.Interfaces
{
    public interface IService<T>
    {
        public List<T> GetAll();

        public T GetById(int id);

        public T Create(T elem);

        public T Update(T elem);

        public bool Delete(int id);

    }
}
