using FluentValidation.TestHelper;
using Jalasoft.Dev33Level2.Classroom.Models;
using Jalasoft.Dev33Level2.Classroom.Models.Validators;
using System;
using Xunit;

namespace Jalasoft.Dev33Level2.Validator.Tests
{
    public class ValidatorUserTest : IClassFixture<UserValidator>
    {
        private readonly UserValidator userValidator;
        public ValidatorUserTest(UserValidator userValidator)
        {
            this.userValidator = userValidator;
        }

        [Fact]
        public void User_With_Email_Invalid()
        {
            User model = new User() { Email = "test.com"};
            var result = userValidator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(user => user.Email).WithErrorMessage("Email address valid is required.");
        }

        [Fact]
        public void User_With_Email_Valid()
        {
            User model = new User() { Email = "test@gmail.com" };
            var result = userValidator.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(user => user.Email);
        }

        [Fact]
        public void User_With_Lastname_Empty()
        {
            User model = new User() { LastName = String.Empty };
            var result = userValidator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(user => user.LastName);
        }

        [Fact]
        public void User_With_Lastname_Null()
        {
            User model = new User() { LastName = null };
            var result = userValidator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(user => user.LastName);
        }

        [Fact]
        public void User_With_Lastname_Valid()
        {
            User model = new User() { LastName = "Herbas" };
            var result = userValidator.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(user => user.LastName);
        }

        [Fact]
        public void User_With_Firstname_Empty()
        {
            User model = new User() { FirstName = String.Empty };
            var result = userValidator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(user => user.FirstName);
        }

        [Fact]
        public void User_With_Firstname_Null()
        {
            User model = new User() { FirstName = null };
            var result = userValidator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(user => user.FirstName);
        }

        [Fact]
        public void User_With_Firstname_Valid()
        {
            User model = new User() { FirstName = "luis" };
            var result = userValidator.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(user => user.FirstName);
        }

        [Fact]
        public void User_With_DateBorn_Invalid()
        {
            User model = new User() { DateBorn = DateTime.Now };
            var result = userValidator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(user => user.DateBorn).WithErrorMessage("Datetime Invalid");
        }

        [Fact]
        public void User_With_DateBorn_Valid()
        {
            User model = new User() { DateBorn = new DateTime(2015, 12, 25) };
            var result = userValidator.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(user => user.DateBorn);
        }

        [Fact]
        public void User_With_Small_Invalid_FirstName_Throw_Exception()
        {
            User model = new User() { FirstName = "k" };
            var result = userValidator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(user => user.FirstName);
        }

    }
}
