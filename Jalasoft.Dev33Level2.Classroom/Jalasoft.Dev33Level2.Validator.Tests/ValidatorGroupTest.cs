﻿using FluentValidation.TestHelper;
using Jalasoft.Dev33Level2.Classroom.Models;
using Jalasoft.Dev33Level2.Classroom.Models.Validators;
using System;
using Xunit;

namespace Jalasoft.Dev33Level2.Validator.Tests
{
    public  class ValidatorGroupTest : IClassFixture<GroupValidator>
    {
        private readonly GroupValidator groupValidator;
        public ValidatorGroupTest(GroupValidator groupValidator)
        {
            this.groupValidator = groupValidator;
        }

        [Fact]
        public void Group_With_Description_Valid()
        {
            Group model = new Group() { Description = "this is a description" };
            var result = groupValidator.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(group => group.Description);
        }

        [Fact]
        public void Group_With_Description_Empty()
        {
            Group model = new Group() { Description = String.Empty };
            var result = groupValidator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(group => group.Description);
        }

        [Fact]
        public void Group_With_Description_Null()
        {
            Group model = new Group() { Description = null };
            var result = groupValidator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(group => group.Description);
        }


        [Fact]
        public void Group_With_Name_Valid()
        {
            Group model = new Group() { Name = "Course" };
            var result = groupValidator.TestValidate(model);
            result.ShouldNotHaveValidationErrorFor(group => group.Name);
        }

        [Fact]
        public void Group_With_Name_Null()
        {
            Group model = new Group() { Name = null };
            var result = groupValidator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(group => group.Name);
        }

        [Fact]
        public void Group_With_Name_Empty()
        {
            Group model = new Group() { Name = String.Empty };
            var result = groupValidator.TestValidate(model);
            result.ShouldHaveValidationErrorFor(group => group.Name);
        }

    }
}
