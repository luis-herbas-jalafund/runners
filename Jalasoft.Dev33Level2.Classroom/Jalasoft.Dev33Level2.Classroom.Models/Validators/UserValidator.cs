﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jalasoft.Dev33Level2.Classroom.Models.Validators
{
    public class UserValidator : AbstractValidator<User>
    {
        public UserValidator()
        {
            RuleFor(user => user.Email).NotEmpty().EmailAddress().WithMessage("Email address valid is required.");
            RuleFor(user => user.FirstName).NotEmpty().NotNull().MaximumLength(25).MinimumLength(3);
            RuleFor(user => user.LastName).NotEmpty().NotNull().MaximumLength(25).MinimumLength(3);
            RuleFor(user => user.DateBorn).NotEmpty().LessThan(DateTime.Now).WithMessage("Datetime Invalid");
            RuleFor(user => user.Gender).GreaterThan(0).LessThan(3);
        }
    }
}
