﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;


namespace Jalasoft.Dev33Level2.Classroom.Models.Validators
{
    public class GroupValidator : AbstractValidator<Group>
    {
        public GroupValidator()
        {
            RuleFor(group => group.Description).NotNull().NotEmpty().MaximumLength(25).MinimumLength(3);
            RuleFor(group => group.Name).NotNull().NotEmpty().MaximumLength(25).MinimumLength(3);
            RuleFor(group => group.MaxUsersQuantity).GreaterThan(0).LessThan(30);
        }
    }
}
