﻿using System;
using System.Collections.Generic;

namespace Jalasoft.Dev33Level2.Classroom.Models
{
    public class Group
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public int MaxUsersQuantity { get; set; }
        public List<User> Users { get; set; }
    }
}
