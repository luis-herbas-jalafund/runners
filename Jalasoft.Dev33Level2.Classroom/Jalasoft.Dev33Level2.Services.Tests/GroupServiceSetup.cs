﻿using Jalasoft.Dev33Level2.Classroom.Models;
using Jalasoft.Dev33Level2.Classroom.Repositories.Interfaces;
using Jalasoft.Dev33Level2.Classroom.Services;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jalasoft.Dev33Level2.Services.Tests
{
    public abstract class GroupServiceSetup
    {
        protected Mock<IRepository<Group>> mockGroupRepository;
        protected GroupService groupService;
        protected List<Group> groups;
        protected Group group;
        public GroupServiceSetup()
        {
            this.SetUpConfig();
        }
        private void SetUpConfig()
        {
            this.mockGroupRepository = new Mock<IRepository<Group>>();
            this.groupService = new GroupService(this.mockGroupRepository.Object);
            this.groups = new List<Group>()
            {
                new Group() { Description = "Backend Group", MaxUsersQuantity = 10, Name = "Backend"},
                new Group() { Description = "Frontend Group", MaxUsersQuantity = 10, Name = "Frontend"},
                new Group() { Description = "DevOps Group", MaxUsersQuantity = 10, Name = "DevOps"},
                new Group() { Description = "Monitoring Group", MaxUsersQuantity = 10, Name = "Monitoring"}
            };
            this.group = new Group() { Description = "English Group", MaxUsersQuantity = 12, Name = "English" };
        }

    }
}
