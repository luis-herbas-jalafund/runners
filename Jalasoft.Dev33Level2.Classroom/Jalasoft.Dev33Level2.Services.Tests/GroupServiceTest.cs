﻿using Jalasoft.Dev33Level2.Classroom.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Jalasoft.Dev33Level2.Services.Tests
{
    public class GroupServiceTest : GroupServiceSetup
    {

        [Fact]
        public void Create_Successfully_Group_Returns_Group()
        {
            this.mockGroupRepository.Setup(groupRepository => groupRepository.Create(It.IsAny<Group>())).Returns(this.group);
            var result = this.groupService.Create(this.group);
            Assert.Equal(this.group, result);
        }

        [Fact]
        public void Create_Empty_Group_Throw_Exception_With_Message()
        {
            var exception = Assert.Throws<Exception>(() => this.groupService.Create(new Group()));
            string msg = "You must specify the description and name";
            Assert.Equal(msg, exception.Message);
        }

        [Fact]
        public void GetAll_Successfully_Returns_List_Of_Groups()
        {
            this.mockGroupRepository.Setup(groupRepository => groupRepository.GetAll()).Returns(this.groups);
            var result = this.groupService.GetAll();
            Assert.Collection(result,
                group1 => Assert.Equal("Backend", group1.Name),
                group2 => Assert.Equal("Frontend", group2.Name),
                group3 => Assert.Equal("DevOps", group3.Name),
                group4 => Assert.Equal("Monitoring", group4.Name));
        }

        [Fact]
        public void GetById_Successfully_Returns_Group()
        {
            this.mockGroupRepository.Setup(groupRepository => groupRepository.GetById(It.IsAny<int>())).Returns(this.group);
            var result = this.groupService.GetById(1);
            Assert.Equal(this.group, result);
        }

        [Fact]
        public void GetById_NonExistent_User_Throw_Exception_With_Message()
        {
            var exception = Assert.Throws<Exception>(() => this.groupService.GetById(100));
            string msg = "Group not Found";
            Assert.Equal(msg, exception.Message);
        }

        [Fact]
        public void Update_NonExistent_Group_Throw_Exception_With_Message()
        {
            var exception = Assert.Throws<Exception>(() => this.groupService.Update(this.group));
            string msg = "Group does not exist.";
            Assert.Equal(msg, exception.Message);
        }

        [Fact]
        public void Update_Invalid_Empty_Group_Throw_Exception_With_Message()
        {
            this.mockGroupRepository.Setup(groupRepository => groupRepository.GetById(It.IsAny<int>())).Returns(this.group);
            var exception = Assert.Throws<Exception>(() => this.groupService.Update(new Group()));
            string msg = "You must specify the description and name";
            Assert.Equal(msg, exception.Message);
        }

        [Fact]
        public void Update_Successfully_Returns_Updated_Group()
        {
            var updateGroup = new Group() { Description = "English Group 10:30", MaxUsersQuantity = 12, Name = "English" };
            this.mockGroupRepository.Setup(groupRepository => groupRepository.GetById(It.IsAny<int>())).Returns(this.group);
            this.mockGroupRepository.Setup(groupRepository => groupRepository.Update(It.IsAny<Group>())).Returns(updateGroup);
            var result = this.groupService.Update(updateGroup);
            Assert.Equal(updateGroup, result);
        }

        [Fact]
        public void Delete_NonExistent_Group_Throw_Exception_With_Message()
        {
            var exception =  Assert.Throws<Exception>(() => this.groupService.Delete(100));
            string msg = "Group does not exist.";
            Assert.Equal(msg, exception.Message);
        }

        [Fact]
        public void Delete_Successfully_Returns_True()
        {
            this.mockGroupRepository.Setup(groupRepository => groupRepository.GetById(It.IsAny<int>())).Returns(this.group);
            this.mockGroupRepository.Setup(groupRepository => groupRepository.Delete(It.IsAny<int>())).Returns(true);
            var result = this.groupService.Delete(1);
            Assert.True(result);
        }

        [Fact]
        public void Delete_Zero_Groups_Returns_False()
        {
            this.mockGroupRepository.Setup(groupRepository => groupRepository.GetById(It.IsAny<int>())).Returns(this.group);
            this.mockGroupRepository.Setup(groupRepository => groupRepository.Delete(It.IsAny<int>())).Returns(false);
            var result = this.groupService.Delete(1);
            Assert.False(result);
        }
    }
}
