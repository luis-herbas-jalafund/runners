﻿using Jalasoft.Dev33Level2.Classroom.Models;
using Jalasoft.Dev33Level2.Classroom.Repositories.Interfaces;
using Jalasoft.Dev33Level2.Classroom.Services;
using Jalasoft.Dev33Level2.Classroom.Services.Interfaces;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jalasoft.Dev33Level2.Services.Tests
{
    public class UserServiceSetup
    {
        protected Mock<IRepository<User>> mockUserRepository;
        protected UserService userService;
        protected List<User> users;
        protected User user;
        public UserServiceSetup()
        {
            this.SetUpConfig();
        }

        private void SetUpConfig()
        {
            this.mockUserRepository = new Mock<IRepository<User>>();
            this.userService = new UserService(this.mockUserRepository.Object);
            this.users = new List<User>()
            {
                new User() { FirstName = "Armando", LastName = "Bronca", Email = "armando.bronca@gmail.com", Gender = 1, GroupId = 1, DateBorn = new DateTime(1998, 2, 11) },
                new User() { FirstName = "Helen", LastName = "Chufe", Email = "helen.chufe@gmail.com", Gender = 0, GroupId = 1, DateBorn = new DateTime(1998, 12, 21) },
                new User() { FirstName = "Lola", LastName = "Mento", Email = "Lola.mento@gmail.com", Gender = 0, GroupId = 1, DateBorn = new DateTime(1998, 5, 7) },
            };
            this.user = new User() { FirstName = "Luis", LastName = "Herbas", Email = "luis.herbas@gmail.com", Gender = 1, GroupId = 1, DateBorn = new DateTime(1998, 3, 22) };
        }
    }
       
}
