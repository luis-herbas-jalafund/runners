using Jalasoft.Dev33Level2.Classroom.Models;
using Jalasoft.Dev33Level2.Classroom.Services;
using Moq;
using System;
using Xunit;

namespace Jalasoft.Dev33Level2.Services.Tests
{
    public class UserServiceTest : UserServiceSetup
    {
        

        [Fact]
        public void Create_Empty_User_Throw_Exception()
        {
            User user = new User();
            Assert.Throws<Exception>(() => this.userService.Create(user));
        }

        // [Fact]
        // public void Create_Empty_User_And_Throw_Exception_With_Validation_Failures_Msg()
        // {
        //     User user = new User();
        //     var exception = Assert.Throws<Exception>(() => this.userService.Create(user));
        //     string msg = "\n 'Email' no deber�a estar vac�o.\n 'First Name' no deber�a estar vac�o.\n " +
        //         "'First Name' no debe estar vac�o.\n 'Last Name' no deber�a estar vac�o.\n 'Last Name' " +
        //         "no debe estar vac�o.\n 'Date Born' no deber�a estar vac�o.\n 'Gender' debe ser mayor que '0'.";
        //     Assert.Equal(msg, exception.Message);
        // }

        [Fact]
        public void Create_Successfully_User_Returns_User()
        {
            this.mockUserRepository.Setup(userRepository => userRepository.Create(It.IsAny<User>())).Returns(this.user);
            var result = this.userService.Create(this.user);
            Assert.Equal(this.user, result);
        }

        [Fact]
        public void Delete_NonExistent_User_Throw_Exception()
        {
            Assert.Throws<Exception>(() => this.userService.Delete(100));
        }

        [Fact]
        public void Delete_NonExistent_User_Throw_Exception_With_Message()
        {
            var exception = Assert.Throws<Exception>(() => this.userService.Delete(100));
            string msg = "User does not exist.";
            Assert.Equal(msg, exception.Message);
        }

        [Fact]
        public void Delete_Successfully_User_Returns_True()
        {
            this.mockUserRepository.Setup(userRepository => userRepository.GetById(It.IsAny<int>())).Returns(this.user);
            this.mockUserRepository.Setup(userRepository => userRepository.Delete(It.IsAny<int>())).Returns(true);
            var result = this.userService.Delete(1);
            Assert.True(result);
        }

        [Fact]
        public void Delete_Zero_Users_Returns_False()
        {
            this.mockUserRepository.Setup(userRepository => userRepository.GetById(It.IsAny<int>())).Returns(this.user);
            this.mockUserRepository.Setup(userRepository => userRepository.Delete(It.IsAny<int>())).Returns(false);
            var result = this.userService.Delete(1);
            Assert.False(result);
        }

        [Fact]
        public void GetAll_Successfully_Returns_List_Of_Users()
        {
            this.mockUserRepository.Setup(userRepository => userRepository.GetAll()).Returns(this.users);
            var result = this.userService.GetAll();
            Assert.Collection(result,
                user1 => Assert.Equal("Armando", user1.FirstName),
                user2 => Assert.Equal("Helen", user2.FirstName),
                user3 => Assert.Equal("Lola", user3.FirstName));
        }

        [Fact]
        public void GetById_Successfully_Returns_User()
        {
            this.mockUserRepository.Setup(userRepository => userRepository.GetById(It.IsAny<int>())).Returns(this.user);
            var result = this.userService.GetById(1);
            Assert.Equal(this.user, result);
        }

        [Fact]
        public void GetById_NonExistent_User_Throw_Exception()
        {
            Assert.Throws<Exception>(() => this.userService.GetById(100));
        }

        [Fact]
        public void Update_NonExistent_User_Throw_Exception()
        {
            Assert.Throws<Exception>(() => this.userService.Update(this.user));
        }

        // [Fact]
        // public void Update_Invalid_Empty_User_Throw_Exception_With_Message()
        // {
        //     this.mockUserRepository.Setup(userRepository => userRepository.GetById(It.IsAny<int>())).Returns(this.user);
        //     var exception = Assert.Throws<Exception>(() => this.userService.Update(new User()));
        //     string msg = "\n 'Email' no deber�a estar vac�o.\n 'First Name' no deber�a estar vac�o.\n " +
        //         "'First Name' no debe estar vac�o.\n 'Last Name' no deber�a estar vac�o.\n 'Last Name' " +
        //         "no debe estar vac�o.\n 'Date Born' no deber�a estar vac�o.\n 'Gender' debe ser mayor que '0'.";
        //     Assert.Equal(msg, exception.Message);
        // }

        [Fact]
        public void Update_Successfully_Returns_Updated_User()
        {
            var updateUser = new User() { FirstName = "Luis", LastName = "Herbas", Email = "luis.herbas@outlook.com", Gender = 1, GroupId = 1, DateBorn = new DateTime(1998, 3, 22) };
            this.mockUserRepository.Setup(userRepository => userRepository.GetById(It.IsAny<int>())).Returns(this.user);
            this.mockUserRepository.Setup(userRepository => userRepository.Update(It.IsAny<User>())).Returns(updateUser);
            var result = this.userService.Update(updateUser);
            Assert.Equal(updateUser, result);
        }
    }
}
