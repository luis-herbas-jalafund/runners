using Jalasoft.Dev33Level2.Classroom.Models;
using Jalasoft.Dev33Level2.Classroom.Repositories;
using Jalasoft.Dev33Level2.Classroom.Repositories.Context;
using Jalasoft.Dev33Level2.Classroom.Repositories.Interfaces;
using Jalasoft.Dev33Level2.Classroom.Services;
using Jalasoft.Dev33Level2.Classroom.Services.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Jalasoft.Dev33Level2.Classroom.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Jalasoft.Dev33Level2.Classroom.Api", Version = "v1" });
            });
            services.AddSingleton<IService<User>, UserService>();
            services.AddSingleton<IService<Group>, GroupService>();
            services.AddSingleton<IRepository<User>, UserRepository>();
            services.AddSingleton<IRepository<Group>, GroupRepository>();
            services.AddTransient<IContext<Group>>(s => new GroupContext(Configuration.GetConnectionString("DBClass")));
            services.AddTransient<IContext<User>>(s => new UserContext(Configuration.GetConnectionString("DBClass")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Jalasoft.Dev33Level2.Classroom.Api v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
