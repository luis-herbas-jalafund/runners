﻿using Jalasoft.Dev33Level2.Classroom.Models;
using Jalasoft.Dev33Level2.Classroom.Services;
using Jalasoft.Dev33Level2.Classroom.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace Jalasoft.Dev33Level2.Classroom.Api.Controllers
{
    [ApiController]
    [Route("api/jalasoft/[controller]")]
    public class UsersController : ControllerBase
    {
        ILogger logger;
        IService<User> service;
        public UsersController(ILogger<User> logger, IService<User> service)
        {
            this.logger = logger;
            this.service = service;
        }

        [HttpGet]
        public IActionResult Get()
        {

            return Ok(service.GetAll());
        }

        [HttpGet("{id}")]
        public IActionResult GetUser(int id)
        {
            try
            {
                var result = service.GetById(id);
                logger.LogInformation($"user with id {id} found");
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.LogWarning(ex.Message);
                return NotFound(ex.Message);

            }

        }

        [ProducesResponseType(typeof(User), 201)]
        [HttpPost]
        public IActionResult PostUser([FromBody] User newUser)
        {
            try
            {
                var result = service.Create(newUser);
                logger.LogInformation($"user created");
                return new ObjectResult(result) { StatusCode = 201 };

            }
            catch (Exception ex)
            {

                logger.LogWarning(ex.Message);
                return new ObjectResult(ex.Message) { StatusCode = 500 }; ;
            }
        }

        [ProducesResponseType(typeof(User), 200)]
        [HttpPut]
        public IActionResult PutUser([FromBody] User user)
        {
            try
            {
                var result = service.Update(user);
                logger.LogInformation($"user updated");
                return new ObjectResult(result) { StatusCode = 200 };

            }
            catch (Exception ex)
            {

                logger.LogWarning(ex.Message);
                return new ObjectResult(ex.Message) { StatusCode = 500 }; ;
            }

        }

        [ProducesResponseType(typeof(bool), 200)]
        [HttpDelete("{id}")]
        public IActionResult DeleteUser(int id)
        {
            try
            {
                var result = service.Delete(id);
                logger.LogInformation($"user deleted");
                return new ObjectResult(result) { StatusCode = 200 };

            }
            catch (Exception ex)
            {
                logger.LogWarning(ex.Message);
                return new ObjectResult(ex.Message) { StatusCode = 500 }; ;
            }

        }

    }
}
