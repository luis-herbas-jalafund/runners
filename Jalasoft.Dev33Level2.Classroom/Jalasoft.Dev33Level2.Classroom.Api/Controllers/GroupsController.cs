﻿using Jalasoft.Dev33Level2.Classroom.Models;
using Jalasoft.Dev33Level2.Classroom.Services;
using Jalasoft.Dev33Level2.Classroom.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Jalasoft.Dev33Level2.Classroom.Api.Controllers
{
    [ApiController]
    [Route("api/jalasoft/[controller]")]
    public class GroupsController : ControllerBase
    {
        ILogger logger;
        IService<Group> service;
        public GroupsController(ILogger<Group> logger, IService<Group> service)
        {
            this.logger = logger;
            this.service = service;
        }

        [HttpGet]
        public IActionResult Get()
        {

            return Ok(service.GetAll());
        }

        [HttpGet("{id}")]
        public IActionResult GetGroup(int id)
        {
            try
            {
                var result = service.GetById(id);
                logger.LogInformation($"group with id {id} found");
                return Ok(result);
            }
            catch (Exception ex)
            {
                logger.LogWarning(ex.Message);
                return NotFound(ex.Message);

            }

        }

        [ProducesResponseType(typeof(Group), 201)]
        [HttpPost]
        public IActionResult PostGroup([FromBody] Group newGroup)
        {
            try
            {
                var result = service.Create(newGroup);
                logger.LogInformation($"group created");
                return new ObjectResult(result) { StatusCode = 201 };

            }
            catch (Exception ex)
            {

                logger.LogWarning(ex.Message);
                return new ObjectResult(ex.Message) { StatusCode = 500 }; ;
            }
        }

        [ProducesResponseType(typeof(Group), 200)]
        [HttpPut]
        public IActionResult PutGroup([FromBody] Group group)
        {
            try
            {
                var result = service.Update(group);
                logger.LogInformation($"group updated");
                return new ObjectResult(result) { StatusCode = 200 };

            }
            catch (Exception ex)
            {

                logger.LogWarning(ex.Message);
                return new ObjectResult(ex.Message) { StatusCode = 500 }; ;
            }

        }

        [ProducesResponseType(typeof(bool), 200)]
        [HttpDelete("{id}")]
        public IActionResult DeleteGroup(int id)
        {
            try
            {
                var result = service.Delete(id);
                logger.LogInformation($"group deleted");
                return new ObjectResult(result) { StatusCode = 200 };

            }
            catch (Exception ex)
            {
                logger.LogWarning(ex.Message);
                return new ObjectResult(ex.Message) { StatusCode = 500 }; ;
            }

        }


    }
}
