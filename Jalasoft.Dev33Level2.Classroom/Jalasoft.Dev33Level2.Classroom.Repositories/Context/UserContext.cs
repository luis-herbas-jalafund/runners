﻿using Dapper;
using Jalasoft.Dev33Level2.Classroom.Models;
using Jalasoft.Dev33Level2.Classroom.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;

namespace Jalasoft.Dev33Level2.Classroom.Repositories.Context
{
    public class UserContext : IContext<User>
    {
        private readonly string connectionString;
        private readonly string tableName;
        public UserContext(string connectionString)
        {
            this.connectionString = connectionString;
            tableName = "jalasoft_user";
        }

        public User Create(User newElement)
        {
            string query = $"INSERT INTO {tableName} OUTPUT INSERTED.Id VALUES(@FirstName, @LastName, @Gender, @DateBorn, @Email, @GroupId)";
            using (IDbConnection dbConnection = new SqlConnection(connectionString))
            {
                int insert = dbConnection.QuerySingle<int>(query, newElement);
                if (insert >= 1)
                {
                    newElement.Id = insert;
                    return newElement;
                }
                throw new Exception("Error, the new user could not be created");
            }
        }

        public bool Delete(int id)
        {
            bool result = false;
            string query = $"DELETE FROM {tableName} OUTPUT DELETED.* WHERE id=@Id ";
            using (IDbConnection dbConnection = new SqlConnection(connectionString))
            {
                int delete = dbConnection.QuerySingle<int>(query, new { ID = id });
                if (delete >= 1)
                {
                    result = true;
                }
            }
            return result;
        }

        public User GetById(int id)
        {
            string query = $"select * from {tableName} where id=@ID";
            using (IDbConnection dbConnection = new SqlConnection(connectionString))
            {
                var res = dbConnection.Query<User>(query, new { ID = id });
                if (res.Count() >= 1)
                {
                    return res.ToList()[0];
                }
                throw new Exception("user not Found");
            }
        }

        public List<User> SelectAll()
        {
            List<User> users = new List<User>();
            using (IDbConnection dbConnection = new SqlConnection(connectionString))
            {
                users = dbConnection.Query<User>($"select * from {tableName}").ToList();
            }
            return users;
        }

        public User Update(User element)
        {
            string query = $"UPDATE {tableName} SET Firstname=@FirstName, Lastname=@LastName, " +
                $"Gender=@Gender, DateBorn=@DateBorn, Email=@Email, GroupId=@GroupId OUTPUT INSERTED.* WHERE id=@Id ";
            using (IDbConnection dbConnection = new SqlConnection(connectionString))
            {
                int insert = dbConnection.QuerySingle<int>(query, element);
                Console.WriteLine(insert);
                if (insert >= 1)
                {
                    return element;
                }
                throw new Exception("Error, the new group could not be updated");
            }
        }
    }
}
