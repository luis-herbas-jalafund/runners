﻿using Dapper;
using Jalasoft.Dev33Level2.Classroom.Models;
using Jalasoft.Dev33Level2.Classroom.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;



namespace Jalasoft.Dev33Level2.Classroom.Repositories.Context
{
    public class GroupContext : IContext<Group>
    {
        private readonly string connectionString;
        private readonly string tableName;

        public GroupContext(string connectionString)
        {
            this.connectionString = connectionString;
            tableName = "jalasoft_group";
        }

        public List<Group> SelectAll()
        {
            List<Group> groups = new List<Group>();
            using (IDbConnection dbConnection = new SqlConnection(connectionString))
            {
                groups = dbConnection.Query<Group>("select * from jalasoft_group").ToList(); 
            }
            return groups;
        }

        public Group GetById(int id)
        {
            string query = $"SELECT * FROM {tableName} WHERE id=@ID";
            using (IDbConnection dbConnection = new SqlConnection(connectionString))
            {
                var group = dbConnection.Query<Group>(query, new {ID = id});
                if (group.Count() >= 1)
                {
                    return group.ToList()[0];
                }
                throw new Exception("Group not Found");
            }
        }

        public Group Create(Group newGroup)
        {
            string query = $"INSERT INTO {tableName} OUTPUT INSERTED.Id VALUES(@Name, @Description, @MaxUsersQuantity)";
            using (IDbConnection dbConnection = new SqlConnection(connectionString))
            {
                int insert = dbConnection.QuerySingle<int>(query, newGroup);
                if (insert >= 1)
                {
                    return newGroup;
                }
                throw new Exception("Error, the new group could not be created");
            }
        }

        public Group Update(Group group)
        {
            string query = $"UPDATE {tableName} SET Name=@Name, Description=@Description, MaxUsersQuantity=@MaxUsersQuantity OUTPUT INSERTED.* WHERE id=@Id ";
            using (IDbConnection dbConnection = new SqlConnection(connectionString))
            {
                int insert = dbConnection.QuerySingle<int>(query, group);
                Console.WriteLine(insert);
                if (insert >= 1)
                {
                    return group;
                }
                throw new Exception("Error, the new group could not be updated");
            }
        }

        public bool Delete(int id)
        {
            bool result = false;
            string query = $"DELETE FROM {tableName} OUTPUT DELETED.* WHERE id=@Id ";
            using (IDbConnection dbConnection = new SqlConnection(connectionString))
            {
                int delete = dbConnection.QuerySingle<int>(query, new { ID = id });
                if (delete >= 1)
                {
                   result = true;
                }
            }
            return result;

        }
    }
}
