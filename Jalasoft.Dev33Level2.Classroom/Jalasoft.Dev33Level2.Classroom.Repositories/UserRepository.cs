﻿using Jalasoft.Dev33Level2.Classroom.Models;
using Jalasoft.Dev33Level2.Classroom.Repositories.Context;
using Jalasoft.Dev33Level2.Classroom.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jalasoft.Dev33Level2.Classroom.Repositories
{
    public class UserRepository : IRepository<User>
    {
        IContext<User> context;
        public UserRepository(IContext<User> context)
        {
            this.context = context;
        }
        public User Create(User newElement)
        {
            return context.Create(newElement);
        }

        public bool Delete(int id)
        {
            return context.Delete(id);
        }

        public List<User> GetAll()
        {
            return context.SelectAll();
        }

        public User GetById(int id)
        {
            return context.GetById(id);
        }

        public User Update(User element)
        {
            return context.Update(element);
        }
    }
}
