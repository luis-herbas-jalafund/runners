﻿using Jalasoft.Dev33Level2.Classroom.Models;
using Jalasoft.Dev33Level2.Classroom.Repositories.Context;
using Jalasoft.Dev33Level2.Classroom.Repositories.Interfaces;
using System;
using System.Collections.Generic;

namespace Jalasoft.Dev33Level2.Classroom.Repositories
{
    public class GroupRepository : IRepository<Group>
    {
        IContext<Group> context;

        public GroupRepository(IContext<Group> context)
        {
            this.context = context;
        }

        public List<Group> GetAll()
        {
            return context.SelectAll();
        }

        public Group GetById(int id)
        {
            return context.GetById(id);
        }

        public Group Create(Group newGroup)
        {
            return context.Create(newGroup);
        }

        public Group Update(Group group)
        {
            return context.Update(group);
        }

        public bool Delete(int id)
        {
            return context.Delete(id);
        }
    }
}
