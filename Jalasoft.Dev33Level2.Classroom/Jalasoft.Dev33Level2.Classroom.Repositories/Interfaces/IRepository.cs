﻿using System;
using System.Collections.Generic;


namespace Jalasoft.Dev33Level2.Classroom.Repositories.Interfaces
{
    public interface IRepository<T>
    {

        public List<T> GetAll();

        public T GetById(int id);

        public T Create(T newElement);
        public T Update(T element);

        public bool Delete(int id);
    }
}
