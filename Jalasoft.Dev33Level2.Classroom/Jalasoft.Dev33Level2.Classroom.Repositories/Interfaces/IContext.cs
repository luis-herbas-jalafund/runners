﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jalasoft.Dev33Level2.Classroom.Repositories.Interfaces
{
    public interface IContext<T>
    {
        public List<T> SelectAll();

        public T GetById(int id);

        public T Create(T newElement);

        public T Update(T element);

        public bool Delete(int id);
    }
}
